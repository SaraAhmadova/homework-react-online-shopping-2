import React, { useState } from "react";
import ProductCard from "../../components/ProductCard";
import ConfirmDeleteModal from "../../components/ConfirmDeleteModal";
import { Header } from "../../components/header";

const CartPage = () => {
  const [productList, setProductList] = useState(
    JSON.parse(localStorage.getItem("cart")) || []
  );
  console.log(productList);
  const [showDeleteModal, setShowDeleteModal] = useState();
  const onOpenDeleteModal = (product) => {
    setShowDeleteModal(product);
  };

  const onCloseConfirmDeleteModal = () => {
    setShowDeleteModal(null);
  };
  const onDeleteProduct = () => {
    const ProductList = (productList.filter(product=>product.sku !== showDeleteModal.sku));
    localStorage.setItem('cart',JSON.stringify(ProductList));
    setProductList(JSON.parse(localStorage.getItem("cart")))
    setShowDeleteModal(null);
  };


  return (
    <div>
      <h1>Cart page</h1>
      <Header />
      <div className="product-list">
        {productList?.length> 0 && productList.map((product, index) => (
          <ProductCard
            key={index}
            product={product}
            isCartProduct={true}
            onDeleteProduct={()=>{onOpenDeleteModal(product)}}
          />
        ))}
      </div>
      {showDeleteModal && (
        <ConfirmDeleteModal
          onCloseConfirmDeleteModal={onCloseConfirmDeleteModal}
          onSubmitDeleteProduct={onDeleteProduct}
        />
      )}
    </div>
  );
};

export default CartPage;
