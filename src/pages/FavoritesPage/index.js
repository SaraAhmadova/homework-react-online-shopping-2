import React, { useState } from "react";
import { Header } from "../../components/header";
import ProductCard from "../../components/ProductCard";

const FavoritesPage = () => {
  const [favProductList, setFavProductList] = useState(
    JSON.parse(localStorage.getItem("favorites")) || []
  );
  const onToggleFavorite = (product) => {
    setFavProductList(favProductList.filter((fav) => fav.sku !== product.sku));
    localStorage.setItem(
      "favorites",
      JSON.stringify(favProductList.filter((fav) => fav.sku !== product.sku))
    );
  };

  return (
    <div>
      <h1>Favorites page</h1>

      <Header />
      <div className="product-list">
        {favProductList.map((product, index) => (
          <ProductCard
            key={index}
            product={product}
            isFavorite={true}
            onToggleFavorite={() => {
              onToggleFavorite(product);
            }}
          />
        ))}
      </div>
    </div>
  );
};

export default FavoritesPage;
