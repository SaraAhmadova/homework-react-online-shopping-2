import React, { useEffect, useState } from "react";
import "./HomePage.css";
import ProductList from "../../components/ProductList";
import { Header } from "../../components/header";

const HomePage = () => {
  const [products, setProducts] = useState([]);
  const [favProducts, setFavProducts] = useState(
    JSON.parse(localStorage.getItem("favorites")) || []
  );
  const [cartProducts, setCartProducts] = useState(
    JSON.parse(localStorage.getItem("cart")) || []
  );

  useEffect(() => {
    fetch("/products.json")
      .then((response) => response.json())
      .then((data) => {
        setProducts(data);
      })
      .catch((error) => {
        console.error("Error fetching data:", error);
      });
  }, []);

  const addToCart = (product) => {
    const existedProduct = cartProducts.find((e) => e.sku === product.sku);
    setCartProducts(
      existedProduct
          ? cartProducts.map((e) => {
              if (e.sku === product.sku) return { ...e, count: e.count + 1 };
              else return { ...e };
            })
        : [...cartProducts, { ...product, count: 1 }]
    );

    localStorage.setItem(
      "cart",
      JSON.stringify(
        existedProduct
          ? cartProducts.map((e) => {
              if (e.sku === product.sku) return { ...e, count: e.count + 1 };
              else return { ...e };
            })
          : [...cartProducts, { ...product, count: 1 }]
      )
    );
  };

  const toggleFavorite = (product) => {
    setFavProducts(
      favProducts.includes(
        favProducts.find((prodct) => prodct.sku === product.sku)
      )
        ? favProducts.filter((item) => item.sku !== product.sku)
        : [...favProducts, product]
    );
    localStorage.setItem(
      "favorites",
      JSON.stringify(
        favProducts.includes(
          favProducts.find((prodct) => prodct.sku === product.sku)
        )
          ? favProducts.filter((item) => item.sku !== product.sku)
          : [...favProducts, product]
      )
    );
  };

  return (
    <div>
      <h1>Online Store</h1>
      <Header
        favoritesCount={favProducts?.length ?? 0}
        cartCount={cartProducts?.length ?? 0}
      />
      {products && (
        <ProductList
          products={products}
          onAddToCart={addToCart}
          onToggleFavorite={toggleFavorite}
          favorites={favProducts}
        />
      )}
    </div>
  );
};

export default HomePage;
