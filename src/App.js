import React from "react";
import "./App.css";
import Router from "./router";

const App = () => {
  return (
    <React.StrictMode>
      <Router />
    </React.StrictMode>
  );
};

export default App;
