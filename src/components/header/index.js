import React from "react";
import "./header.css";
import { Link } from "react-router-dom";

export const Header = (props) => {
  return (
    <ul className={"list-container"}>
      <li className={"list-item"}>
        <button>
          <Link to={"/"}>
            <span>Home</span>
          </Link>
        </button>
      </li>
      <li className={"list-item"}>
        <button>
          <Link to={"/favorites"}>
            <span role="img" aria-label="favorites">
              ⭐
            </span>
            <span>Favorites</span>
          </Link>
        </button>
      </li>
      <li className={"list-item"}>
        <button>
          <Link to={"/cart"}>
            <span role="img" aria-label="cart">
              🛒
            </span>
            <span>Cart</span>
          </Link>
        </button>
      </li>
    </ul>
  );
};
