import React from "react";
import PropTypes from "prop-types";
import ProductCard from "./ProductCard";

const ProductList = ({
  products,
  onAddToCart,
  onToggleFavorite,
  favorites,
  isCart,
}) => {
  return (
    <div className="product-list">
      {products.map((product) => (
        <ProductCard
          key={product.sku}
          product={product}
          onAddToCart={onAddToCart}
          onToggleFavorite={onToggleFavorite}
          isCartProcut={isCart}
          isFavorite={
            favorites
              ? favorites.includes(
                  favorites.find((fav) => fav.sku === product.sku)
                )
              : false
          }
        />
      ))}
    </div>
  );
};

ProductList.propTypes = {
  products: PropTypes.array.isRequired,
  onAddToCart: PropTypes.func,
  onToggleFavorite: PropTypes.func,
  favorites: PropTypes.array,
  isCart: PropTypes.bool,
};
export default ProductList;
