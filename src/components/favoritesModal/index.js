import React from "react";
import PropTypes from "prop-types";
import "./favoritesModal.css";

const FavoritesModal = ({ isOpen, onClose, favorites }) => {

  return (
    isOpen && (
      <div className="favorites-modal" onClick={onClose}>
        <div
          className="favorites-modal-content"
          onClick={(e) => e.stopPropagation()}
        >
          <span className="favorites-modal-close" onClick={onClose}>
            &times;
          </span>
          <h2>Favorites</h2>
          <ul>
            {favorites.map((product, index) => {
              console.log(favorites);
              return (
                <li key={index}>
                  {product.name} - ${product.price}
                </li>
              );
            })}
          </ul>
        </div>
      </div>
    )
  );
};

FavoritesModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  favorites: PropTypes.array.isRequired,
};
export default FavoritesModal;
