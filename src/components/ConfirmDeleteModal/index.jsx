import React from 'react'
import './ConfirmDeleteModal.css';

const ConfirmDeleteModal = ({onCloseConfirmDeleteModal, onSubmitDeleteProduct}) => {
    
  return (
    <div className="cart-modal" onClick={onCloseConfirmDeleteModal}>
    <div className="cart-modal-content" onClick={(e) => e.stopPropagation()}>
      <span className="cart-modal-close" onClick={onCloseConfirmDeleteModal}>
        &times;
      </span>
      <h2>Are you sure to delete product from basket?</h2>
      <button onClick={onCloseConfirmDeleteModal}>Cancel</button>
      <button onClick={onSubmitDeleteProduct}>Yes</button>
    </div>
  </div>
  )
}

export default ConfirmDeleteModal