import HomePage from "../pages/HomePage";
import CartPage from "../pages/CartPage";
import FavoritesPage from "../pages/FavoritesPage";
import { createBrowserRouter } from "react-router-dom";

export const router = createBrowserRouter([
  {
    path: "/",
    element: <HomePage />,
  },
  {
    path: "/cart",
    element: <CartPage />,
  },
  {
    path: "/favorites",
    element: <FavoritesPage />,
  },
]);